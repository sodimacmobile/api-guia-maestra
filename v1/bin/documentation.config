<?xml version="1.0"?>
<doc>
    <assembly>
        <name>API</name>
    </assembly>
    <members>
        <member name="T:API.WebApiConfig">
            <summary>
            WEB API Global Configuration
            </summary>
        </member>
        <member name="F:API.WebApiConfig.RootRoles">
            <summary>
            Roles with Administrator Privileges
            </summary>
        </member>
        <member name="M:API.WebApiConfig.Register(System.Web.Http.HttpConfiguration)">
            <summary>
            Register Config Variables
            </summary>
            <param name="config"></param>
        </member>
        <member name="T:API.Endpoints.Accounts.AccountsController">
            <summary>
            Account API
            </summary>
        </member>
        <member name="M:API.Endpoints.Accounts.AccountsController.Get">
            <summary>
            Retrieve Account's
            </summary>
            <returns></returns>
        </member>
        <member name="M:API.Endpoints.Accounts.AccountsController.Get(System.String)">
            <summary>
            Retrieve Target Account Information
            </summary>
            <param name="id">Account Token</param>
            <returns></returns>
        </member>
        <member name="M:API.Endpoints.Accounts.AccountsController.Current">
            <summary>
            Retrieve Current Account Information
            </summary>
            <returns></returns>
        </member>
        <member name="M:API.Endpoints.Accounts.AccountsController.Post(API.Endpoints.Accounts.Models.Create)">
            <summary>
            Pre-Register an account in the system
            </summary>
            <param name="account">Account information</param>
            <returns></returns>
        </member>
        <member name="M:API.Endpoints.Accounts.AccountsController.Put(System.String,API.Endpoints.Accounts.Models.Update)">
            <summary>
            Update the target Account
            </summary>
            <param name="id">Account Token</param>
            <param name="account">Account information</param>
            <returns></returns>
        </member>
        <member name="M:API.Endpoints.Accounts.AccountsController.ActivateAccount(System.String)">
            <summary>
            Activate an Account
            </summary>
            <param name="account"></param>
            <returns></returns>
        </member>
        <member name="M:API.Endpoints.Accounts.AccountsController.DeactivateAccount(System.String)">
            <summary>
            Deactivate an Account
            </summary>
            <param name="account"></param>
            <returns></returns>
        </member>
        <member name="M:API.Endpoints.Accounts.AccountsController.RecoverPassword(API.Endpoints.Accounts.Models.RecoverPassword)">
            <summary>
            Send an email for a user , to recover his 'credentials'
            </summary>
            <param name="data">Recover Model</param>
            <returns></returns>
        </member>
        <member name="M:API.Endpoints.Accounts.AccountsController.Register(API.Endpoints.Accounts.Models.Register)">
            <summary>
            Register User (Confirmation via Email)
            </summary>
            <param name="register">Register Model</param>
            <returns></returns>
        </member>
        <member name="T:API.Endpoints.Accounts.Services.Create">
            <summary>
            Add User to DB
            </summary>
        </member>
        <member name="M:API.Endpoints.Accounts.Services.Create.#ctor(API.Endpoints.Accounts.Models.Create,System.String)">
            <summary>
            Constructor
            </summary>
            <param name="user">New User Information</param>
            <param name="host">Application URL</param>
        </member>
        <member name="M:API.Endpoints.Accounts.Services.Create.TemplateBody(System.Object)">
            <summary>
            
            </summary>
            <param name="model"></param>
            <returns></returns>
        </member>
        <member name="M:API.Endpoints.Accounts.Services.Create.ExecuteAsync(System.Threading.CancellationToken)">
            <summary>
            Async Process
            </summary>
            <param name="cancellationToken"></param>
            <returns></returns>
        </member>
        <member name="T:API.Endpoints.Accounts.Services.Get">
            <summary>
            Retrieve a Target User
            </summary>
        </member>
        <member name="M:API.Endpoints.Accounts.Services.Get.#ctor(System.String)">
            <summary>
            Constructor
            </summary>
            <param name="token"></param>
        </member>
        <member name="M:API.Endpoints.Accounts.Services.Get.ExecuteAsync(System.Threading.CancellationToken)">
            <summary>
            Async Process
            </summary>
            <param name="cancellationToken"></param>
            <returns></returns>
        </member>
        <member name="T:API.Endpoints.Accounts.Services.RecoverPassword">
            <summary>
            Send And Email to the user for recovering Password
            </summary>
        </member>
        <member name="M:API.Endpoints.Accounts.Services.RecoverPassword.#ctor(System.String,System.String)">
            <summary>
            Constructor
            </summary>
            <param name="token">User Token</param>
            <param name="user">Target Model</param>
        </member>
        <member name="M:API.Endpoints.Accounts.Services.RecoverPassword.TemplateBody(System.Object)">
            <summary>
            
            </summary>
            <param name="model"></param>
            <returns></returns>
        </member>
        <member name="M:API.Endpoints.Accounts.Services.RecoverPassword.ExecuteAsync(System.Threading.CancellationToken)">
            <summary>
            
            </summary>
            <param name="cancellationToken"></param>
            <returns></returns>
        </member>
        <member name="T:API.Endpoints.Accounts.Services.Register">
            <summary>
            Register User in DB (By Register Email)
            </summary>
        </member>
        <member name="M:API.Endpoints.Accounts.Services.Register.#ctor(System.String,API.Endpoints.Accounts.Models.Register)">
            <summary>
            Constructor
            </summary>
            <param name="token">User Token</param>
            <param name="user">Target Model</param>
        </member>
        <member name="M:API.Endpoints.Accounts.Services.Register.ExecuteAsync(System.String,System.Threading.CancellationToken)">
            <summary>
             Update User
            </summary>
            <param name="token"></param>
            <param name="cancellationToken"></param>
            <returns></returns>
        </member>
        <member name="T:API.Endpoints.Accounts.Services.Update">
            <summary>
            Update User in DB
            </summary>
        </member>
        <member name="M:API.Endpoints.Accounts.Services.Update.#ctor(System.String,API.Endpoints.Accounts.Models.Update)">
            <summary>
            Constructor
            </summary>
            <param name="token">User Token</param>
            <param name="user">Target Model</param>
        </member>
        <member name="M:API.Endpoints.Accounts.Services.Update.ExecuteAsync(System.String,System.Threading.CancellationToken)">
            <summary>
             Update User
            </summary>
            <param name="token"></param>
            <param name="cancellationToken"></param>
            <returns></returns>
        </member>
        <member name="T:API.Endpoints.Accounts.Templates.Mail.Mail">
            <summary>
              A strongly-typed resource class, for looking up localized strings, etc.
            </summary>
        </member>
        <member name="P:API.Endpoints.Accounts.Templates.Mail.Mail.ResourceManager">
            <summary>
              Returns the cached ResourceManager instance used by this class.
            </summary>
        </member>
        <member name="P:API.Endpoints.Accounts.Templates.Mail.Mail.Culture">
            <summary>
              Overrides the current thread's CurrentUICulture property for all
              resource lookups using this strongly typed resource class.
            </summary>
        </member>
        <member name="P:API.Endpoints.Accounts.Templates.Mail.Mail.Recover_Subject">
            <summary>
              Looks up a localized string similar to Recuperación de Contraseña.
            </summary>
        </member>
        <member name="P:API.Endpoints.Accounts.Templates.Mail.Mail.Register_Subject">
            <summary>
              Looks up a localized string similar to Activa tu cuenta.
            </summary>
        </member>
        <member name="T:API.Endpoints.Brands.BrandsController">
            <summary>
            Brands Controller
            </summary>
        </member>
        <member name="M:API.Endpoints.Brands.BrandsController.Get">
            <summary>
            Get all Brands (ODATA Convention)
            </summary>
            <returns></returns>
        </member>
        <member name="T:API.Endpoints.Categories.CategoriesController">
            <summary>
            Categories Controller
            </summary>
        </member>
        <member name="M:API.Endpoints.Categories.CategoriesController.Get">
            <summary>
            Get all categories (ODATA Convention's)
            </summary>
            <returns></returns>
        </member>
        <member name="M:API.Endpoints.Categories.CategoriesController.GetSections(System.String)">
            <summary>
            Get all section from a specific category (ODATA Convention's)
            </summary>
            <param name="category">Category Token</param>
            <returns></returns>
        </member>
        <member name="M:API.Endpoints.Categories.CategoriesController.GetProductsBySection(System.String,System.String,System.Int32,System.Int32)">
            <summary>
            Get all products from a specific section (ODATA Convention's)
            </summary>
            <param name="category">Category token</param>
            <param name="section">Section token</param>
            <param name="$limit">Pagination limit</param>
            <param name="$offset">Pagination offset</param>
            <returns></returns>
        </member>
        <member name="T:API.Endpoints.Categories.Services.GetProductsBySection">
            <summary>
            Get All Products By Section (Paginated)
            </summary>
        </member>
        <member name="M:API.Endpoints.Categories.Services.GetProductsBySection.#ctor(System.Int32,System.Int32,System.String,System.String)">
            <summary>
            Constructor
            </summary>
            <param name="limit">Pagination limit</param>
            <param name="offset">Pagination offset</param>
            <param name="user">Current User</param>
            <param name="section">Section token</param>
        </member>
        <member name="M:API.Endpoints.Categories.Services.GetProductsBySection.ExecuteAsync(System.Threading.CancellationToken)">
            <summary>
            Async Process
            </summary>
            <param name="cancellationToken"></param>
            <returns></returns>
        </member>
        <member name="T:API.Endpoints.Country.CountriesController">
            <summary>
            Countries Controllers
            </summary>
        </member>
        <member name="M:API.Endpoints.Country.CountriesController.Get">
            <summary>
            Get all Countries (ODATA Convention's)
            </summary>
            <returns></returns>
        </member>
        <member name="M:API.Endpoints.Country.CountriesController.GetCommunesByCountry(System.String)">
            <summary>
            Get all States (Communes) for a Country (ODATA Convention's)
            </summary>
            <returns></returns>
        </member>
        <member name="M:API.Endpoints.Country.CountriesController.GetCommunesByCountryIdentifier(System.String)">
            <summary>
            Get all States (Communes) for a Country (ODATA Convention's)
            </summary>
            <returns></returns>
        </member>
        <member name="T:API.Endpoints.Files.FilesController">
            <summary>
            File API
            </summary>
        </member>
        <member name="M:API.Endpoints.Files.FilesController.Get(System.String)">
            <summary>
            Retrieves a File Content
            </summary>
            <returns></returns>
        </member>
        <member name="M:API.Endpoints.Files.FilesController.Post">
            <summary>
            Create a Temporary File  (Must be Change the flag to permanently after)
            </summary>
            <returns></returns>
        </member>
        <member name="T:API.Endpoints.Files.Services.Upload">
            <summary>
            File Upload
            </summary>
        </member>
        <member name="M:API.Endpoints.Files.Services.Upload.#ctor(System.Net.Http.HttpRequestMessage,System.String)">
            <summary>
            Constructor
            </summary>
            <param name="request">Http Request</param>
            <param name="userID">User ID</param>
        </member>
        <member name="M:API.Endpoints.Files.Services.Upload.SaveFiles(System.Collections.Generic.List{System.Net.Http.HttpContent})">
            <summary>
            Save Files into DB
            </summary>
            <param name="files"></param>
            <returns></returns>
        </member>
        <member name="T:API.Endpoints.Files.Services.View">
            <summary>
            Authentication API
            </summary>
        </member>
        <member name="M:API.Endpoints.Files.Services.View.#ctor(System.String)">
            <summary>
            Constructor
            </summary>
            <param name="token">Token del Archivo</param>
        </member>
        <member name="M:API.Endpoints.Files.Services.View.ExecuteAsync(System.Threading.CancellationToken)">
            <summary>
            Obtiene la foto del usuario
            </summary>
            <param name="cancellationToken"></param>
            <returns></returns>
        </member>
        <member name="T:API.Endpoints.Locals.LocalsController">
            <summary>
            Local's Controllers
            </summary>
        </member>
        <member name="M:API.Endpoints.Locals.LocalsController.Get">
            <summary>
            Get all Locals (ODATA Convention's)
            </summary>
            <returns></returns>
        </member>
        <member name="M:API.Endpoints.Locals.LocalsController.GetPricesFromProducts(System.String,System.String[])">
            <summary>
            Get the prices from a list of product's associated with the local
            </summary>
            <returns></returns>
        </member>
        <member name="T:API.Endpoints.Locals.Services.GetPricesFromProducts">
            <summary>
            Get the prices from a list of product , for a specific local
            </summary>
        </member>
        <member name="M:API.Endpoints.Locals.Services.GetPricesFromProducts.#ctor(System.String,System.String,System.Collections.Generic.List{API.Endpoints.Locals.Models.Product})">
            <summary>
            Constructor
            </summary>
            <param name="user">User Token</param>
            <param name="local">Local Token</param>
            <param name="products">List of product to extract the prices</param>
        </member>
        <member name="M:API.Endpoints.Locals.Services.GetPricesFromProducts.ExecuteAsync(System.Threading.CancellationToken)">
            <summary>
            Async Process
            </summary>
            <param name="cancellationToken"></param>
            <returns></returns>
        </member>
        <member name="T:API.Endpoints.Products.ProductsController">
            <summary>
            Product Controller 
            </summary>
        </member>
        <member name="M:API.Endpoints.Products.ProductsController.Get(System.String)">
            <summary>
            Get product details (ODATA Convention's)
            </summary>
            <returns></returns>
        </member>
        <member name="M:API.Endpoints.Products.ProductsController.GetPhoto(System.String)">
            <summary>
            Get product photo
            </summary>
            <returns></returns>
        </member>
        <member name="M:API.Endpoints.Products.ProductsController.GetPrices(System.String)">
            <summary>
            Get all prices from each local for a specific product
            </summary>
            <returns></returns>
        </member>
        <member name="T:API.Endpoints.Products.Services.GetPrices">
            <summary>
            Get All Prices (for each local) for a specific product 
            </summary>
        </member>
        <member name="M:API.Endpoints.Products.Services.GetPrices.ExecuteAsync(System.Threading.CancellationToken)">
            <summary>
            Async Process
            </summary>
            <param name="cancellationToken"></param>
            <returns></returns>
        </member>
        <member name="T:API.Endpoints.Products.Services.Photo">
            <summary>
            Get Default Photo from a Product
            </summary>
        </member>
        <member name="M:API.Endpoints.Products.Services.Photo.#ctor(System.String)">
            <summary>
            Constructor
            </summary>
            <param name="product">Product token</param>
        </member>
        <member name="M:API.Endpoints.Products.Services.Photo.ExecuteAsync(System.Threading.CancellationToken)">
            <summary>
            Obtiene la foto del usuario
            </summary>
            <param name="cancellationToken"></param>
            <returns></returns>
        </member>
        <member name="T:API.Endpoints.Roles.RolesController">
            <summary>
            Profiles Controller
            </summary>
        </member>
        <member name="M:API.Endpoints.Roles.RolesController.Get">
            <summary>
            Retrieve Account's
            </summary>
            <returns></returns>
        </member>
        <member name="T:API.Endpoints.Security.Resources.Security">
            <summary>
              A strongly-typed resource class, for looking up localized strings, etc.
            </summary>
        </member>
        <member name="P:API.Endpoints.Security.Resources.Security.ResourceManager">
            <summary>
              Returns the cached ResourceManager instance used by this class.
            </summary>
        </member>
        <member name="P:API.Endpoints.Security.Resources.Security.Culture">
            <summary>
              Overrides the current thread's CurrentUICulture property for all
              resource lookups using this strongly typed resource class.
            </summary>
        </member>
        <member name="P:API.Endpoints.Security.Resources.Security.USERNAME_OR_PASSWORD_INCORRECT">
            <summary>
              Looks up a localized string similar to Usuario o Clave Incorrecta.
            </summary>
        </member>
        <member name="T:API.Endpoints.Security.SecurityController">
            <summary>
            Security Controller to grant JWT to Valid User's
            </summary>
        </member>
        <member name="M:API.Endpoints.Security.SecurityController.Authorize(API.Endpoints.Security.Models.Credentials)">
            <summary>
            Authorize user by credentials
            </summary>
            <param name="credentials">Credentials</param>
            <returns></returns>
            <response code="200">Authorized</response>
            <response code="500">Incorrect Username or Password</response>
        </member>
        <member name="T:API.Endpoints.Security.Services.Authorize">
            <summary>
            Authorize an User by Credentials
            </summary>
        </member>
        <member name="M:API.Endpoints.Security.Services.Authorize.#ctor(System.Net.Http.HttpRequestMessage,API.Endpoints.Security.Models.Credentials)">
            <summary>
            Constructor
            </summary>
            <param name="request"></param>
            <param name="credentials"></param>
        </member>
        <member name="M:API.Endpoints.Security.Services.Authorize.ExecuteAsync(System.Threading.CancellationToken)">
            <summary>
            Async Process
            </summary>
            <param name="cancellationToken"></param>
            <returns></returns>
        </member>
        <member name="T:API.Errors">
            <summary>
              A strongly-typed resource class, for looking up localized strings, etc.
            </summary>
        </member>
        <member name="P:API.Errors.ResourceManager">
            <summary>
              Returns the cached ResourceManager instance used by this class.
            </summary>
        </member>
        <member name="P:API.Errors.Culture">
            <summary>
              Overrides the current thread's CurrentUICulture property for all
              resource lookups using this strongly typed resource class.
            </summary>
        </member>
        <member name="P:API.Errors.COUNTRY_REQUIRED">
            <summary>
              Looks up a localized string similar to Country is required.
            </summary>
        </member>
        <member name="P:API.Errors.EMAIL_REQUIRED">
            <summary>
              Looks up a localized string similar to Email is required.
            </summary>
        </member>
        <member name="P:API.Errors.EMPTY_BODY">
            <summary>
              Looks up a localized string similar to Model is Empty.
            </summary>
        </member>
        <member name="P:API.Errors.PASSWORD_REQUIRED">
            <summary>
              Looks up a localized string similar to Password is required.
            </summary>
        </member>
        <member name="T:API.WebApiApplication">
            <summary>
            Web Api Bootstrap
            </summary>
        </member>
        <member name="M:API.WebApiApplication.Application_Start">
            <summary>
            Start up
            </summary>
        </member>
    </members>
</doc>
