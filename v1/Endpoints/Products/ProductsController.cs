﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace API.Endpoints.Products
{
    /// <summary>
    /// Product Controller 
    /// </summary>
    public class ProductsController : Gale.REST.RestController
    {
        /// <summary>
        /// Get product details (ODATA Convention's)
        /// </summary>
        /// <returns></returns>
        [HierarchicalRoute("{product:Guid}")]
        public IHttpActionResult Get(String product)
        {
            //By Identifier
            var settings = new Gale.REST.Queryable.OData.Builders.GQLConfiguration();
            settings.filters.Add(new Gale.REST.Queryable.OData.Builders.GQLConfiguration.Filter()
            {
                field = "token",
                operatorAlias = "eq",
                value = product
            });

            return new Gale.REST.Http.HttpQueryableActionResult<Models.Product>(this.Request, settings);
        }

        #region --> PHOTO
        /// <summary>
        /// Get product photo
        /// </summary>
        /// <returns></returns>
        [HierarchicalRoute("{product:Guid}/Photo")]
        public IHttpActionResult GetPhoto(String product)
        {
            return new Services.Photo(product);
        }
        #endregion

        #region --> PRICES
        /// <summary>
        /// Get all prices from each local for a specific product
        /// </summary>
        /// <returns></returns>
        [HierarchicalRoute("{product:Guid}/Prices")]
        public IHttpActionResult GetPrices(String product)
        {
            var user = Guid.NewGuid().ToString();
            return new Services.GetPrices(user, product);
        }
        #endregion
    }
}