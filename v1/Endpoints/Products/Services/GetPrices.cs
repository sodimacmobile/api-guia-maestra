﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace API.Endpoints.Products.Services
{
    /// <summary>
    /// Get All Prices (for each local) for a specific product 
    /// </summary>
    public class GetPrices : Gale.REST.Http.HttpReadActionResult<String>
    {
        String _user;

        public GetPrices(String user, String product)
            : base(product)
        {
            _user = user;
        }

        /// <summary>
        /// Async Process
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            //----------------------------------------------------------------------------------------------------
            //Guard Exception's
            Gale.Exception.RestException.Guard(() => this.Model == null, "EMPTY_PRODUCT", API.Errors.ResourceManager);
            //----------------------------------------------------------------------------------------------------

            using (Gale.Db.DataService svc = new Gale.Db.DataService("[PA_SOD_OBT_PreciosProducto]"))
            {
                svc.Parameters.Add("PROD_Token", this.Model);
                svc.Parameters.Add("ENTI_Token", this._user);

                Gale.Db.EntityRepository rep = this.ExecuteQuery(svc);
                var items = rep.GetModel<Models.Price>();


                //----------------------------------------------------------------------------------------------------
                //Create Response
                var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new ObjectContent<Object>(
                        items,
                        System.Web.Http.GlobalConfiguration.Configuration.Formatters.KqlFormatter()
                    )
                };

                //Return Task
                return Task.FromResult(response);
                //----------------------------------------------------------------------------------------------------

            }
        }
    }
}