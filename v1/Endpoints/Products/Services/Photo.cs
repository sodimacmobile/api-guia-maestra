﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace API.Endpoints.Products.Services
{
    /// <summary>
    /// Get Default Photo from a Product
    /// </summary>
    public class Photo: Gale.REST.Http.HttpBaseActionResult
    {
        string _product;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="product">Product token</param>
        public Photo(String product)
        {
            _product = product;
        }

        /// <summary>
        /// Obtiene la foto del usuario
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override Task<HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            //------------------------------------------------------------------------------------------------------
            // DB Execution
            using (Gale.Db.DataService svc = new Gale.Db.DataService("PA_SOD_OBT_ImagenProducto"))
            {
                svc.Parameters.Add("PROD_Token", _product);

                Gale.Db.EntityRepository rep = this.ExecuteQuery(svc);

                Models.FileData file = rep.GetModel<Models.FileData>().FirstOrDefault();

                if (file == null)
                {
                    return Task.FromResult(new HttpResponseMessage(System.Net.HttpStatusCode.NotFound));
                }

                //Create Response
                var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new StreamContent(new System.IO.MemoryStream(file.binary.ToArray())),

                };

                //Cache Control Time (in Minutes)
                int cacheInMinutes = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Sodimac:Image:Cache"]);

                //CACHE CONTROL (WORK IN PRODUCTION STAGE)
                response.Headers.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue()
                {
                    MaxAge = TimeSpan.FromMinutes(cacheInMinutes),
                    Public = true
                };

                //Add Content-Type Header
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(file.contentType);

                return Task.FromResult(response);
            }
            //------------------------------------------------------------------------------------------------------

        }

    }
}