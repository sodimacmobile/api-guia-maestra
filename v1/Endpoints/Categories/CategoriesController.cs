﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace API.Endpoints.Categories
{
    /// <summary>
    /// Categories Controller
    /// </summary>
    public class CategoriesController : Gale.REST.RestController
    {
        #region Categories
        /// <summary>
        /// Get all categories (ODATA Convention's)
        /// </summary>
        /// <returns></returns>
        [Swashbuckle.Swagger.Annotations.QueryableEndpoint(typeof(Models.Category))]
        public IHttpActionResult Get()
        {
            return new Gale.REST.Http.HttpQueryableActionResult<Models.Category>(this.Request);
        }

        #endregion

        #region --> SECTIONS

        /// <summary>
        /// Get all section from a specific category (ODATA Convention's)
        /// </summary>
        /// <param name="category">Category Token</param>
        /// <returns></returns>
        [HierarchicalRoute("{category:Guid}/Sections")]
        public IHttpActionResult GetSections(String category)
        {
            //By Identifier
            var settings = new Gale.REST.Queryable.OData.Builders.GQLConfiguration();
            settings.filters.Add(new Gale.REST.Queryable.OData.Builders.GQLConfiguration.Filter()
            {
                field = "category_token",
                operatorAlias = "eq",
                value = category
            });

            return new Gale.REST.Http.HttpQueryableActionResult<Models.Section>(this.Request, settings);
        }
        #endregion

        #region --> SECTIONS > PRODUCTS

        /// <summary>
        /// Get all products from a specific section (ODATA Convention's)
        /// </summary>
        /// <param name="category">Category token</param>
        /// <param name="section">Section token</param>
        /// <param name="$limit">Pagination limit</param>
        /// <param name="$offset">Pagination offset</param>
        /// <returns></returns>
        [HierarchicalRoute("{category:Guid}/Sections/{section:Guid}/Products")]
        public IHttpActionResult GetProductsBySection(String category, String section, [FromUri(Name = "$limit")]int limit = 10, [FromUri(Name = "$offset")]int offset = 0)
        {
            var user = Guid.NewGuid().ToString();
            return new Services.GetProductsBySection(limit, offset, user, section);
        }
        #endregion
    }


}