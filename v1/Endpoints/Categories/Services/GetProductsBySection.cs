﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace API.Endpoints.Categories.Services
{
    /// <summary>
    /// Get All Products By Section (Paginated)
    /// </summary>
    public class GetProductsBySection : Gale.REST.Http.HttpReadActionResult<String>
    {
        int _limit;
        int _offset;
        String _user;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="limit">Pagination limit</param>
        /// <param name="offset">Pagination offset</param>
        /// <param name="user">Current User</param>
        /// <param name="section">Section token</param>
        public GetProductsBySection(int limit, int offset,String user, String section) : base(section) {
            _limit = limit;
            _offset = offset;
            _user = user;
        }

        /// <summary>
        /// Async Process
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            //----------------------------------------------------------------------------------------------------
            //Guard Exception's
            Gale.Exception.RestException.Guard(() => this.Model == null, "EMPTY_SECTION", API.Errors.ResourceManager);
            //----------------------------------------------------------------------------------------------------

            using (Gale.Db.DataService svc = new Gale.Db.DataService("[PA_SOD_OBT_ProductosSeccion]"))
            {
                svc.Parameters.Add("SECC_Token", this.Model);
                svc.Parameters.Add("ENTI_Token", this._user);

                //PAGINATION VALUES
                svc.Parameters.Add("Limit", this._limit);
                svc.Parameters.Add("Offset", this._offset);


                Gale.Db.EntityRepository rep = this.ExecuteQuery(svc);

                var pagination = rep.GetModel<Models.Pagination>().FirstOrDefault();
                var items = rep.GetModel<Models.Product>(1);

                pagination.items = items;

                //----------------------------------------------------------------------------------------------------
                //Create Response
                var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new ObjectContent<Object>(
                        pagination,
                        System.Web.Http.GlobalConfiguration.Configuration.Formatters.KqlFormatter()
                    )
                };

                //Return Task
                return Task.FromResult(response);
                //----------------------------------------------------------------------------------------------------

            }
        }
    }
}