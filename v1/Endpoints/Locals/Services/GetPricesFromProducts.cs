﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;

namespace API.Endpoints.Locals.Services
{
    /// <summary>
    /// Get the prices from a list of product , for a specific local
    /// </summary>
    public class GetPricesFromProducts : Gale.REST.Http.HttpReadActionResult<List<Models.Product>>
    {
        String _user;
        String _local;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="user">User Token</param>
        /// <param name="local">Local Token</param>
        /// <param name="products">List of product to extract the prices</param>
        public GetPricesFromProducts(String user, String local, List<Models.Product> products)
            : base(products)
        {
            _user = user;
            _local = local;
        }


        /// <summary>
        /// Async Process
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            //----------------------------------------------------------------------------------------------------
            //Guard Exception's
            Gale.Exception.RestException.Guard(() => this.Model == null, "EMPTY_LOCAL", API.Errors.ResourceManager);
            //----------------------------------------------------------------------------------------------------

            using (Gale.Db.DataService svc = new Gale.Db.DataService("[PA_SOD_OBT_PrecioProductosPorLocal]"))
            {
                svc.Parameters.Add("LOCA_Token", this._local);
                svc.Parameters.Add("ENTI_Token", this._user);
                svc.AddTableType<Models.Product>("Productos", this.Model);


                Gale.Db.EntityRepository rep = this.ExecuteQuery(svc);
                var items = rep.GetModel<Models.ProductPrice>();
                var local = rep.GetModel<Models.Local>(1).FirstOrDefault();


                //----------------------------------------------------------------------------------------------------
                // SEARCH THE PRICES IN THE PMR REST SERVICE!
                #region PMR EXTRACTION!
                try { 
                var skus_to_search = new List<String>();
                items.ForEach((product) =>
                {
                    var sku = product.product_sku.Replace("-", "");
                    skus_to_search.Add(sku);
                });

                using (var client = new HttpClient())
                {
                    String domain = System.Configuration.ConfigurationManager.AppSettings["Sodimac:PMR:BaseURL"];
                    String endpoint = System.Configuration.ConfigurationManager.AppSettings["Sodimac:PMR:LectorDePrecio"];
                    client.BaseAddress = new Uri(domain);
                    string json = JsonConvert.SerializeObject(new
                    {
                        req = new
                        {
                            h = new
                            {
                                atr = new
                                {
                                    p = System.Configuration.ConfigurationManager.AppSettings["Sodimac:PMR:Country"],
                                    e = System.Configuration.ConfigurationManager.AppSettings["Sodimac:PMR:Entity"],
                                    t = local.pmr,
                                    a = System.Configuration.ConfigurationManager.AppSettings["Sodimac:PMR:Application"]
                                }
                            },
                            b = new
                            {
                                cod = skus_to_search.ToArray()
                            }
                        }
                    });

                    HttpResponseMessage Service = client.PostAsync(endpoint, new StringContent(json, Encoding.UTF8, "application/json")).Result;
                    if (Service.IsSuccessStatusCode)
                    {
                        var data = Service.Content.ReadAsAsync<Models.PMR>().Result;

                        //Mapping the PMR Price!
                        data.res.b.productos.ForEach((pmr_product) =>
                        {
                            //Find the matched product!
                            var item = (from product 
                                           in items 
                                       where 
                                       product.product_sku.Replace("-","") == pmr_product.sku
                                            select product).FirstOrDefault();

                            //UPDATE THE ORIGIN AND THE PRICE =) FROM THE PMR SERVICE
                            if (item != null)
                            {
                                item.origin = "PMR";
                                item.price = pmr_product.precio1;
                            }

                        });

                        #region ASYNC MODE FOR PERFOMANCE: UPDATE PMR PRICE TO THE DB
                        //Update PMR Values into the GuiaMaestra DB in Async Mode =)!
                        //Fire And Forget Pattern!
                        //Task.Factory.StartNew(() =>
                        //{
                            var updatedPrices = new List<Models.ProductPriceUpdate>();
                            items.ForEach((product) =>
                            {
                                updatedPrices.Add(new Models.ProductPriceUpdate()
                                {
                                     price =product.price,
                                     token = product.product_token
                                });
                            });

                            using (Gale.Db.DataService upd_svc = new Gale.Db.DataService("[PA_SOD_ACT_PrecioProductosPorLocal]"))
                            {
                                upd_svc.Parameters.Add("LOCA_Token", this._local);
                                upd_svc.Parameters.Add("ENTI_Token", this._user);
                                upd_svc.AddTableType<Models.ProductPriceUpdate>("Productos", updatedPrices);

                                this.ExecuteAction(upd_svc);
                            }
                        //});
                        #endregion
                    }
                }
                }
                catch (Exception ex)
                {
                    //TODO: Save in a Error Log the FAILURE CONNECTION WITH PMR SERVICE??
                    //      Or throw the error to the client??
                }
                #endregion
                //----------------------------------------------------------------------------------------------------



                //----------------------------------------------------------------------------------------------------
                //Create Response
                var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new ObjectContent<Object>(
                        items,
                        System.Web.Http.GlobalConfiguration.Configuration.Formatters.KqlFormatter()
                    )
                };

                //Return Task
                return Task.FromResult(response);
                //----------------------------------------------------------------------------------------------------

            }
        }
    }
}