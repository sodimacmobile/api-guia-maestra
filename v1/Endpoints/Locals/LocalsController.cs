﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace API.Endpoints.Locals
{
    /// <summary>
    /// Local's Controllers
    /// </summary>
    public class LocalsController : Gale.REST.RestController
    {
        /// <summary>
        /// Get all Locals (ODATA Convention's)
        /// </summary>
        /// <returns></returns>
        [Swashbuckle.Swagger.Annotations.QueryableEndpoint(typeof(Models.Local))]
        public IHttpActionResult Get()
        {
            return new Gale.REST.Http.HttpQueryableActionResult<Models.Local>(this.Request);
        }

        #region PRICES FOR THE PRODUCTS (BULK)
        /// <summary>
        /// Get the prices from a list of product's associated with the local
        /// </summary>
        /// <returns></returns>
        [HttpPatch]
        [HierarchicalRoute("{local:Guid}/Prices")]
        public IHttpActionResult GetPricesFromProducts(String local, [FromBody]String[] products)
        {
            #region VALIDATION PRE-PROCESS
            List<Models.Product> products_to_extract = new List<Models.Product>();
            foreach (var product in products)
            {
                //Validate Guid Format for each Product , before Call The Service 
                Gale.Exception.RestException.Guard(() => !product.isGuid(), "PRODUCT_TOKEN_BADFORMAT", product);
                products_to_extract.Add(new Models.Product()
                {
                    token = Guid.Parse(product)
                });
            }
            #endregion

            var user = Guid.NewGuid().ToString();
            return new Services.GetPricesFromProducts(user, local, products_to_extract);
        }
        #endregion
    }
}