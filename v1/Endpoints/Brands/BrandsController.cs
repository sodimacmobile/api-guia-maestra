﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace API.Endpoints.Brands
{
    /// <summary>
    /// Brands Controller
    /// </summary>
    public class BrandsController: Gale.REST.RestController
    {
        /// <summary>
        /// Get all Brands (ODATA Convention)
        /// </summary>
        /// <returns></returns>
        [Swashbuckle.Swagger.Annotations.QueryableEndpoint(typeof(Models.Brand))]
        public IHttpActionResult Get()
        {
            return new Gale.REST.Http.HttpQueryableActionResult<Models.Brand>(this.Request);
        }

    }
}