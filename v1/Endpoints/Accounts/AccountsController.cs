﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace API.Endpoints.Accounts
{
    /// <summary>
    /// Account API
    /// </summary>
    [Gale.Security.Oauth.Jwt.Authorize]
    public class AccountsController : Gale.REST.RestController
    {

        #region ACCOUNT
        /// <summary>
        /// Retrieve Account's
        /// </summary>
        /// <returns></returns>
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.OK)]
        [Gale.Security.Oauth.Jwt.Authorize(Roles = API.WebApiConfig.RootRoles)]
        public IHttpActionResult Get()
        {
            return new Gale.REST.Http.HttpQueryableActionResult<Models.VT_Account>(this.Request);
        }

        /// <summary>
        /// Retrieve Target Account Information
        /// </summary>
        /// <param name="id">Account Token</param>
        /// <returns></returns>
        [HttpGet]
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.OK)]
        [Gale.Security.Oauth.Jwt.Authorize(Roles = API.WebApiConfig.RootRoles)]
        public IHttpActionResult Get(String id)
        {
            //------------------------------------------------------------------------------------------------------
            // GUARD EXCEPTIONS
            Gale.Exception.RestException.Guard(() => !id.isGuid(), "ID_INVALID_GUID", API.Errors.ResourceManager);
            //------------------------------------------------------------------------------------------------------
            return new Services.Get(id);
        }

        /// <summary>
        /// Retrieve Current Account Information
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HierarchicalRoute("/Me")]
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.OK)]
        public IHttpActionResult Current()
        {
            return new Services.Get(this.User.PrimarySid());
        }

        /// <summary>
        /// Pre-Register an account in the system
        /// </summary>
        /// <param name="account">Account information</param>
        /// <returns></returns>
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.Created)]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.BadRequest)]
        [Gale.Security.Oauth.Jwt.Authorize(Roles = API.WebApiConfig.RootRoles)]
        public IHttpActionResult Post([FromBody]Models.Create account)
        {
            string host = this.Request.Headers.Referrer.ToString();
            return new Services.Create(account, host);
        }

        /// <summary>
        /// Update the target Account
        /// </summary>
        /// <param name="id">Account Token</param>
        /// <param name="account">Account information</param>
        /// <returns></returns>
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.NoContent)]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.BadRequest)]
        public IHttpActionResult Put([FromUri]String id, [FromBody]Models.Update account)
        {

            return new Services.Update(id, account);
        }
        #endregion

        #region --> ACTIVE AND DEACTIVATE ACCOUNT

        /// <summary>
        /// Activate an Account
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        [HttpPost]
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.NoContent)]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.BadRequest)]
        [HierarchicalRoute("{account:Guid}/Activate")]
        public IHttpActionResult ActivateAccount(String account)
        {
            string user = this.User.PrimarySid();
            return new Services.Activate(user, account);
        }

        /// <summary>
        /// Deactivate an Account
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        [HttpDelete]
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.NoContent)]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.BadRequest)]
        [HierarchicalRoute("{account:Guid}/Activate")]
        public IHttpActionResult DeactivateAccount(String account)
        {
            string user = this.User.PrimarySid();
            return new Services.Deactivate(user, account);
        }


        #endregion

        #region --> RECOVERING AND REGISTER
        /// <summary>
        /// Send an email for a user , to recover his 'credentials'
        /// </summary>
        /// <param name="data">Recover Model</param>
        /// <returns></returns>
        [HttpPut]
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.NoContent)]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.BadRequest)]
        [HierarchicalRoute("/RecoverPassword")]
        public IHttpActionResult RecoverPassword(Models.RecoverPassword data)
        {
            string host = this.Request.Headers.Referrer.ToString();
            return new Services.RecoverPassword(data.email, host);
        }

        /// <summary>
        /// Register User (Confirmation via Email)
        /// </summary>
        /// <param name="register">Register Model</param>
        /// <returns></returns>
        [HttpPut]
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.NoContent)]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.BadRequest)]
        [HierarchicalRoute("/Register/Confirm")]
        [Gale.Security.Oauth.Jwt.Authorize]
        public IHttpActionResult Register(Models.Register register)
        {
            return new Services.Register(User.PrimarySid(), register);
        }
        #endregion

    }
}