﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace API.Endpoints.Country
{
    /// <summary>
    /// Countries Controllers
    /// </summary>
    public class CountriesController : Gale.REST.RestController
    {

        /// <summary>
        /// Get all Countries (ODATA Convention's)
        /// </summary>
        /// <returns></returns>
        [Swashbuckle.Swagger.Annotations.QueryableEndpoint(typeof(Models.Country))]
        public IHttpActionResult Get()
        {
            return new Gale.REST.Http.HttpQueryableActionResult<Models.Country>(this.Request);
        }

        #region --> STATES (COMMUNES)
        /// <summary>
        /// Get all States (Communes) for a Country (ODATA Convention's)
        /// </summary>
        /// <returns></returns>
        [HierarchicalRoute("{country:Guid}/States")]
        public IHttpActionResult GetCommunesByCountry(String country)
        {
            var settings = new Gale.REST.Queryable.OData.Builders.GQLConfiguration();
            settings.filters.Add(new Gale.REST.Queryable.OData.Builders.GQLConfiguration.Filter()
            {
                field = "country_token",
                operatorAlias = "eq",
                value = country
            });

            return new Gale.REST.Http.HttpQueryableActionResult<Models.Commune>(this.Request, settings);
        }

        /// <summary>
        /// Get all States (Communes) for a Country (ODATA Convention's)
        /// </summary>
        /// <returns></returns>
        [HierarchicalRoute("{identifier}/States")]
        public IHttpActionResult GetCommunesByCountryIdentifier(String identifier)
        {
            var settings = new Gale.REST.Queryable.OData.Builders.GQLConfiguration();
            settings.filters.Add(new Gale.REST.Queryable.OData.Builders.GQLConfiguration.Filter()
            {
                field = "country_identifier",
                operatorAlias = "eq",
                value = identifier
            });

            return new Gale.REST.Http.HttpQueryableActionResult<Models.Commune>(this.Request, settings);
        }
        #endregion
    }
}